var ffmpeg = require('fluent-ffmpeg');
var Jimp = require("jimp");
var util = require('util'),
  child_process = require('child_process');
  var exec = child_process.exec;
  var PropertiesReader = require('properties-reader');
  const getDuration = require('get-video-duration');

exports.imageToVideo = function(){
//var ffmpeg = require('fluent-ffmpeg');
  ffmpeg('/home/hari/calvin.jpg')
    .loop(1)
    .duration(5)
	  .videoBitrate('4000')
    .on('error',function(err){
      console.log('An error occurred: ' + err.message);
    })

    .on('end', function() {
      console.log('Merging finished !');
			//res.send({message:'success'});
    })
    .mergeToFile('/home/hari/output.mp4','/home/hari');
};

exports.stichVideo = function(){
	ffmpeg("/home/hari/Day 1/Vibe_opening shots/1.mp4")
      .mergeAdd("/home/hari/output2.mp4")
    .on('error',function(err){
      console.log('An error occurred: ' + err.message);
    })

    .on('end', function() {
      console.log('Merging finished !');
      //res.send({message:'success'});
    })
    .mergeToFile('/home/hari/finaloutput.mp4','/home/hari');
};


exports.reSizeImage = function(){
    Jimp.read("/home/hari/calvin1.jpg").then(function (image) {
      image.resize(1920, 1080)            // resize 
      .quality(60)                 // set JPEG quality 
      .greyscale()                 // set greyscale 
      .write("/home/hari/calvin.jpg"); // save 
      }).catch(function (err) {
        console.error(err);
    });
};

exports.addAudio = function(){
  /*getDuration('/finaloutput.mp4').then((duration) => {
    console.log(duration);
  });*/
  function puts(error, stdout, stderr) {
  stdout ? util.print('stdout: ' + stdout) : null;
  stderr ? util.print('stderr: ' + stderr) : null;
  error ? console.log('exec error: ' + error) : null;
}
  exec("ffmpeg -i /home/hari/finaloutput.mp4 -i /home/hari/Californication.mp3 -ss 00:00:00 -t 00:00:29 -map 0:0 -map 1:0 /home/hari/output1-file-name.mp4", puts);
};
