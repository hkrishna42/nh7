var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

//MongoDB
mongoose.connect('mongodb://localhost/nh7');

//Express
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Routes
app.use('/api',require('./routes/api'));



//Routes
app.use('/api',require('./routes/api'));

//Start Server
app.listen(3004);
console.log('Listening on port 3004');
