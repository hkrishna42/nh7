// Dependencies
var express = require('express');
var router = express.Router();

//Models
var Video = require('../models/video');


// Routes
router.get('/createvideo',Video.imageToVideo);
router.get('/stichvideo',Video.stichVideo);
router.get('/resizeimage',Video.reSizeImage);
router.get('/addaudio',Video.addAudio);
// Return router
module.exports = router;


